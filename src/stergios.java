import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.commons.math3.linear.LUDecomposition;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import java.sql.*;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.TreeMap;

import java.util.Random;


public class stergios {

	/**
	 * @param args
	 * @throws ClassNotFoundException 
	 * @throws IOException 
	 * @throws NumberFormatException 
	 */
	public static void main(String[] args) throws ClassNotFoundException, NumberFormatException, IOException {
//		final String DB_USERNAME = "root";
//		final String DB_PASSWORD = "";
//		final String DB_HOST = "localhost";// 31.172.245.250
//		final String DB_PORT = "3306";
//		final String DB_NAME = "Evaluation";
//		final String DB_URL = String.format("jdbc:mysql://%s:%s/%s",
//				DB_HOST, DB_PORT, DB_NAME); // DB URL in which we are connecting
		final Integer NEIGHBORHOODSIZE = 4;
		final Integer MINIMUMRATINGTHRESHOLD = 1;
		final Integer crossFoldValidationTimes = 2;
		final int NUMOFRECOMMENDATIONS = 2;
		final boolean geoSocialRecDataset = true; 
		final boolean USERUSER= false; //We change to false if we want to test Item_Item
		final int PICKUSERUSERSIMILARITYMETRIC = 1; //1 for Jaccard Similarity, 2 for Pearson Correlation
		final int PICKITEMITEMSIMILARITYMETRIC =4; //1 for Cosine Similarity, 2 for Simrank, 3 for Prank, 4 for Omnirank
		final int NUMBEROFUSERSFORHETRECDATATASET =60; //0 - 2112
		final int NUMBEROFMOVIESFORHETRECDATATASET = 100; //0 - 10197
		HashMap<Integer, Double> precisionForEachUser = new HashMap<Integer, Double>();
		HashMap<Integer, Double> recallForEachUser = new HashMap<Integer, Double>();
		HashMap<Integer, Double> fScoreForEachUser = new HashMap<Integer, Double>();
		String csvFile = "/User_Location_Activity_Rate_TimeStamp.csv";
		String csvFileHetRec = "/xuser_ratedmovies.csv";
		String filePathHetRec = new File("").getAbsolutePath()+csvFileHetRec;
		String filePath = new File("").getAbsolutePath()+csvFile;
		String friendshipFile = new File("").getAbsolutePath()+"/Friendship_network.csv";
		String xmovieGenresFile = new File("").getAbsolutePath()+"/Xmovie_genres.csv";
		String genreMovieSimilarity = new File("").getAbsolutePath()+"/genreSimilarity.txt";
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ",";
		ArrayList<Integer> userIDTrainingSet= new ArrayList<Integer>();
		ArrayList<Integer> locationIDTrainingSet = new ArrayList<Integer>();
		ArrayList<Integer> activityIDTrainingSet = new ArrayList<Integer>();
		ArrayList<Integer> ratingTrainingSet = new ArrayList<Integer>();
		ArrayList<Integer> friendshipFrom = new ArrayList<Integer>();
		ArrayList<Integer> friendshipTo = new ArrayList<Integer>();
		HashMap<Integer, ArrayList<String>> movieGenres = new HashMap<Integer, ArrayList<String>>();
		HashMap<String, HashMap<String, Double>> genreSimilarityFromFile = new HashMap<String, HashMap<String, Double>>(); 
		//createGenreGenreSimilarityMeasureBasedOnCosineSimilarity();
		br = new BufferedReader(new FileReader(genreMovieSimilarity));
		
		
		
		if(geoSocialRecDataset)
		{
		try {
		
			System.out.println(filePath);
			br = new BufferedReader(new FileReader(filePath));
			
			while ((line = br.readLine()) != null) {
				
			        // use comma as separator
				String[] USER_ID = line.split(cvsSplitBy);
				if(!userIDTrainingSet.contains(Integer.valueOf(USER_ID[0])))
		userIDTrainingSet.add(Integer.valueOf(USER_ID[0]));
				if(!locationIDTrainingSet.contains(Integer.valueOf(USER_ID[1])))
		locationIDTrainingSet.add(Integer.valueOf(USER_ID[1]));
				if(!activityIDTrainingSet.contains(Integer.valueOf(USER_ID[2])))
		activityIDTrainingSet.add(Integer.valueOf(USER_ID[2]));
				if(!ratingTrainingSet.contains(Integer.valueOf(USER_ID[3])))
		ratingTrainingSet.add(Integer.valueOf(USER_ID[3]));

			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		br.close();
		br = new BufferedReader(new FileReader(friendshipFile));
		while ((line = br.readLine()) != null) {

	        // use comma as separator
		String[] USER_ID = line.split(cvsSplitBy);
		friendshipFrom.add(Integer.valueOf(USER_ID[0]));
		friendshipTo.add(Integer.valueOf(USER_ID[1]));
		}
		
		}	
		else
		{
			try
			{
			
			br = new BufferedReader(new FileReader(filePathHetRec));
			int counter = 0;
//			while ((line = br.readLine()) != null) {
//				
//				if(counter == 80)
//				{
//					counter =0;
//			        // use comma as separator
//				String[] USER_ID = line.split(cvsSplitBy);
//				if(!userIDTrainingSet.contains(Integer.valueOf(USER_ID[0])))
//		userIDTrainingSet.add(Integer.valueOf(USER_ID[0]));
//				if(!locationIDTrainingSet.contains(Integer.valueOf(USER_ID[1])))
//		locationIDTrainingSet.add(Integer.valueOf(USER_ID[1]));
//				if(!ratingTrainingSet.contains(Integer.valueOf(USER_ID[2])))
//		ratingTrainingSet.add(Integer.valueOf(USER_ID[2]));
//
//			}
//				else
//				{
//					counter ++;
//				}
//			}
			System.out.println("To userIDTRainingSet einai "+userIDTrainingSet.size());
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		br.close();
		br = new BufferedReader(new FileReader(xmovieGenresFile));
		while ((line = br.readLine()) != null) {
			String[] USER_ID = line.split(cvsSplitBy);
			if(movieGenres.containsKey(USER_ID[1]))
			{
				if(!movieGenres.get(USER_ID[1]).contains(USER_ID[2]))
				{
					movieGenres.get(USER_ID[1]).add(USER_ID[2]);
				}
			}
			else
			{
				ArrayList<String> genresOfEachMovie = new ArrayList<String>();
				genresOfEachMovie.add(USER_ID[2]);
				movieGenres.put(Integer.valueOf(USER_ID[1]), genresOfEachMovie);
			}
			
		}
		
		System.out.println("To System.out.println(locationIDTrainingSet); einai :"+locationIDTrainingSet);
		
		
		}
		Double precisionSum = 0.0;
		Double recallSum=0.0;
		Double fScoreSum=0.0;
	
		
		
		try {
			
			long currentTime = System.currentTimeMillis();
			
			for(int y=0;y<crossFoldValidationTimes;y++)
			{
				ArrayList<Record> trainingDataSet = new ArrayList<Record>();
				ArrayList<Record> testDataSet = new ArrayList<Record>();
			if(geoSocialRecDataset)
				splitDataIntoTrainingTest(filePath,userIDTrainingSet,locationIDTrainingSet, trainingDataSet, testDataSet, NUMBEROFUSERSFORHETRECDATATASET, NUMBEROFMOVIESFORHETRECDATATASET, geoSocialRecDataset,y);
			else
				splitDataIntoTrainingTest(filePathHetRec,userIDTrainingSet,locationIDTrainingSet, trainingDataSet, testDataSet, NUMBEROFUSERSFORHETRECDATATASET, NUMBEROFMOVIESFORHETRECDATATASET, geoSocialRecDataset,y);
			RecommendationTechniques rt = new RecommendationTechniques();
			EvaluationTechniques et = new EvaluationTechniques();
			EvaluationClass ec = new EvaluationClass();
			if(USERUSER)
			{
			for(int i=0;i<userIDTrainingSet.size();i++)// locationIDTrainingSet for ItemItem
			{
			
			ArrayList<Integer> topNProposals= new ArrayList<Integer>(); 
			
			
				rt.User_UserCollaborativeFiltering(topNProposals, userIDTrainingSet.get(i),trainingDataSet, NEIGHBORHOODSIZE, NUMOFRECOMMENDATIONS, MINIMUMRATINGTHRESHOLD, PICKUSERUSERSIMILARITYMETRIC);
				ec= et.countPrecisionUserBasedCF(topNProposals,userIDTrainingSet.get(i),testDataSet);
				System.out.println("To topNProposals einai :"+topNProposals);
				precisionForEachUser.put(userIDTrainingSet.get(i),ec.getPrecision());//
				System.out.println("To ec.getPrecision einai :"+ec.getPrecision());
				recallForEachUser.put(userIDTrainingSet.get(i), ec.getRecall());// LocationId for ItemItem
				fScoreForEachUser.put(userIDTrainingSet.get(i), ec.getFScore());//
			}
			}
			else
			{
				for(int i=0;i<locationIDTrainingSet.size();i++)// locationIDTrainingSet for ItemItem
				{
				System.out.println("To megethos toy training set einai "+trainingDataSet.size());
				ArrayList<Integer> topNProposals= new ArrayList<Integer>(); 
			rt.Item_ItemCollaborativeFiltering(topNProposals,locationIDTrainingSet.get(i),trainingDataSet, NUMOFRECOMMENDATIONS, MINIMUMRATINGTHRESHOLD, PICKITEMITEMSIMILARITYMETRIC, friendshipFrom, friendshipTo, movieGenres, geoSocialRecDataset, genreMovieSimilarity);
			ec= et.countPrecisionItemBasedCF(topNProposals, locationIDTrainingSet.get(i), trainingDataSet, testDataSet);
			precisionForEachUser.put(locationIDTrainingSet.get(i),ec.getPrecision());//
			recallForEachUser.put(locationIDTrainingSet.get(i), ec.getRecall());// LocationId for ItemItem
			fScoreForEachUser.put(locationIDTrainingSet.get(i), ec.getFScore());//
			}
			//	System.out.println("To precision gia ton xrhsth: "+users.get(i)+ " einai "+precisionPerUser);
			}
			
			
			System.out.println("To precision einai :"+precisionForEachUser);
			System.out.println(recallForEachUser);
			System.out.println(fScoreForEachUser);
			ec = calculateAverages(precisionForEachUser, recallForEachUser, fScoreForEachUser);
			
			precisionSum+=ec.getPrecision();
			recallSum+=ec.getRecall();
			fScoreSum+=ec.getFScore();
			}
			
			long finalTime = System.currentTimeMillis();
			System.out.println("O xronos ekteleshs einai: "+(finalTime-currentTime));
			System.out.println ("O mesos oros tou precision einai: "+precisionSum/crossFoldValidationTimes +"\n O mesos oros tou recall einai : "+recallSum/crossFoldValidationTimes+"\n O mesos oros tou fScore einai : "+fScoreSum/crossFoldValidationTimes);
		
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public static void calculateSimrankMatrices(double[][] matrixData,WebGraph wg)
	{RealMatrix I = MatrixUtils.createRealIdentityMatrix(69696);
	RealMatrix I2 = MatrixUtils.createRealIdentityMatrix(wg.numNodes());
		RealMatrix W;
		//We create the W matrix which is the transition probability matrix
		W=MatrixUtils.createRealMatrix(matrixData);
		System.out.println(W);
		//We calculate the kronecker product W' x W'
		KroneckerProduct kp=new KroneckerProduct(W.transpose(), W.transpose());
		//Calculate inverseOf[I-c(W' x W')]
RealMatrix pinverse = new LUDecomposition(I.subtract(kp.calculateProduct().scalarMultiply(0.8))).getSolver().getInverse();
RealMatrix S = pinverse.scalarMultiply(0.2).multiply(kp.vectorization(I2));		
		System.out.println(S);
S=kp.deVectorization(pinverse.scalarMultiply(0.2).multiply(kp.vectorization(I2)));		
		System.out.println(S);
		
	}
	
	
	public void runSimrank()
	{
	WebGraph wg = new WebGraph();
	
		
		wg.addLink("C", "m1", 1.00);

		wg.addLink("C", "m2", 1.00);

		wg.addLink("C", "m3", 1.00);

		wg.addLink("m1", "p1", 1.00);

		wg.addLink("m2", "p1", 1.00);

		wg.addLink("m2", "p3", 1.00);

		wg.addLink("m2", "p4", 1.00);

		wg.addLink("m3", "p4", 1.00);
		
		wg.addLink("p3", "p4", 1.00);
		
		wg.addLink("p4", "p3", 1.00);
		
		double[][] matrixData = new double[wg.numNodes()][wg.numNodes()];
		
		
//		matrixData[0][1]=1.00;
//		
//		matrixData[0][2]=0.50;
//		matrixData[1][3]=1.00;
//		matrixData[3][0]=1.00;
//		matrixData[2][4]=1.00;
//		matrixData[4][2]=0.50;
		
		
		for (int i=1; i<=wg.numNodes();i++)
		{
			
			for(Object outLinksLink1: wg.outLinks(i).keySet())
			{
				int y = (Integer) outLinksLink1;
				matrixData[i-1][y-1]= 1.00/wg.inLinks(y).size() ;
			}
		}
			
		//calculateSimrankMatrices( matrixData, wg);
		
		
		for(int i =1; i<=wg.numNodes();i++)
			System.out.println(i+" : " +wg.IdentifyerToURL(i));
		//define parallel links
		
//		for (int i=1; i<=wg.numNodes();i++)
//		{
//			
//			for(Object outLinksLink1: wg.outLinks(i).keySet())
//			{
//				
//				if(wg.IdentifyerToURL(Integer.valueOf(outLinksLink1.toString())).contains(wg.IdentifyerToURL(i).substring(0, 1)))
//						
//					System.out.println("vrhkame parallel link: " + wg.IdentifyerToURL(Integer.valueOf(outLinksLink1.toString())) + " me "+wg.IdentifyerToURL(i));
//			}
//		}
	//SimRank sm = new SimRank(wg);
	//sm.simrankScores();
	//sm.calculateOutLinks();
	
	
	//System.out.println(sm.simRank("M", "m1"));
		//for(int i=0; i<wg.numNodes();i++)
			//System.out.println(sm.simRank(link));
	
	}
	public static void splitDataIntoTrainingTest(String filePath, ArrayList<Integer> users, ArrayList<Integer> items, ArrayList<Record> trainingDataSet, ArrayList<Record> testDataSet, int nUMBEROFUSERSFORHETRECDATATASET, int nUMBEROFMOVIESFORHETRECDATATASET, boolean geoSocialRecDataset , int fold) throws SQLException, ClassNotFoundException, NumberFormatException, IOException
	{
		


final Double trainingPercent = 0.8;




//We get all the elements from the table
//String delete="DELETE FROM TRAINING_SET";
//statement.executeUpdate(delete);
//delete="DELETE FROM TEST_SET";
//statement.executeUpdate(delete);
//ResultSet rs= statement.executeQuery("Select * FROM `TABLE 1`");

HashMap<Record, Integer> records = new HashMap<Record, Integer>();
ArrayList<Integer> numbers = new ArrayList<Integer>();
ArrayList<Integer> itemsToBeReturned = new ArrayList<Integer>();
BufferedReader br = new BufferedReader(new FileReader(filePath));
String line = "";
String cvsSplitBy = ",";
int counter = 0;
while ((line = br.readLine()) != null) {

    // use comma as separator
String[] USER_ID = line.split(cvsSplitBy);
if(numbers.size() <= nUMBEROFUSERSFORHETRECDATATASET)
{
	if(!numbers.contains(Integer.valueOf(USER_ID[0])))
		//We store the different user id's in an array
		numbers.add(Integer.valueOf(USER_ID[0]));
}

}
br = new BufferedReader(new FileReader(filePath));
counter =0;
while ((line = br.readLine()) != null) {

        // use comma as separator
	String[] USER_ID = line.split(cvsSplitBy);
	if(itemsToBeReturned.size() <= nUMBEROFMOVIESFORHETRECDATATASET && numbers.contains(Integer.valueOf(USER_ID[0])))
	{
		if(!itemsToBeReturned.contains(Integer.valueOf(USER_ID[1])))
			//We store the different location id's in an array
			itemsToBeReturned.add(Integer.valueOf(USER_ID[1]));
		
	
	}
		
}

br = new BufferedReader(new FileReader(filePath));
while ((line = br.readLine()) != null) {

    // use comma as separator
String[] USER_ID = line.split(cvsSplitBy);
Record record;
if (geoSocialRecDataset)
{
 record = new Record(Integer.valueOf(USER_ID[0]),Integer.valueOf(USER_ID[1]),Integer.valueOf(USER_ID[2]),Integer.valueOf(USER_ID[3]));
}
else
{
	 record = new Record(Integer.valueOf(USER_ID[0]),Integer.valueOf(USER_ID[1]),Integer.valueOf(USER_ID[2]),Integer.valueOf(USER_ID[2]));
}

if(numbers.contains(Integer.valueOf(USER_ID[0]))&&itemsToBeReturned.contains(Integer.valueOf(USER_ID[1])))
records.put(record, Integer.valueOf(USER_ID[0]));
}

int sum=0;


//For each different user id, we create a dataset
int count=0;
for(int i=0;i<numbers.size();i++)
{
	users.add(numbers.get(i));
	count=count+1;
	ArrayList<Record> trainingSetByUser = new ArrayList<Record>();
	
	for (Record key:records.keySet())
	{
		
		if(records.get(key).equals(numbers.get(i)))
		{
			//we seperate entries by the user id
			
			trainingSetByUser.add(key);
		}
		
		
	}
System.out.println(" O artihmos twn xrhstwn pou ypologizontai einai " + numbers.size());
	System.out.println("O arithmos xrhsth einai "+numbers.get(i)+" kai to plithos me auton ton arithmo einai "+trainingSetByUser.size());
	ArrayList<Integer> randomNumbers = new ArrayList<Integer>();
	ArrayList<Record> trainingSetNumbers = new ArrayList<Record>();
	ArrayList<Record> testSetNumbers = new ArrayList<Record>();
	Random random = new Random(fold);
	//We select random entries from the training set
	
	
	while(randomNumbers.size()<Math.round(trainingPercent*trainingSetByUser.size()))
	{
		int rand = random.nextInt(trainingSetByUser.size());
		if(!randomNumbers.contains(rand))
			randomNumbers.add(rand);
	}
	for(int x=0;x<trainingSetByUser.size();x++)
	{
	if(randomNumbers.contains(x))
		trainingSetNumbers.add(trainingSetByUser.get(x));
	else
		testSetNumbers.add(trainingSetByUser.get(x));
	}
	System.out.println("Oi synolikoi arithmoi einai "+ trainingSetByUser.size()+", enw ta training set numbers gia ton arithmo "+numbers.get(i)+" einai "+trainingSetNumbers.size()+ ", enw ta test set numbers einai "+ testSetNumbers.size());
	sum = sum +trainingSetByUser.size();
	
for(int y=0;y<trainingSetNumbers.size();y++)
{
	
			trainingDataSet.add(trainingSetNumbers.get(y));
	
	}

for(int y=0;y<testSetNumbers.size();y++)
{
			
			testDataSet.add(testSetNumbers.get(y));
		
		}
}

	
	System.out.println("ede jimcfreunmn : "+testDataSet.size());
	//System.out.println(randomNums);

for (int y=0;y<itemsToBeReturned.size();y++)
{
	items.add(itemsToBeReturned.get(y));
}

	}
	
	public static EvaluationClass calculateAverages(HashMap<Integer , Double> precision, HashMap<Integer , Double> recall, HashMap<Integer , Double> fScore) 
	
	{
		Double precisionSum=0.0;
		Double recallSum=0.0;
		Double fScoreSum=0.0;
		fScore.get(2);
		Double count = (double) precision.size();
		for(Integer key :precision.keySet())
		{
			if(precision.get(key) == null)
				count= count -1;
			else
			{
				precisionSum+=precision.get(key);
				recallSum+=recall.get(key);
				fScoreSum+=fScore.get(key);
						
			}
		}
		
		return new EvaluationClass(precisionSum/count, recallSum/count, fScoreSum/count);
		
	}
	public static void createGenreGenreSimilarityMeasureBasedOnCosineSimilarity() throws NumberFormatException, IOException
	{
		HashMap <String, Double>  perGenreSimilarity = new HashMap<String, Double>();
		HashMap<String, HashMap<String, Double>> genreSimilarity = new HashMap<String, HashMap<String, Double>>();
		HashMap<String, ArrayList<Integer>> movieGenres = new HashMap<String, ArrayList<Integer>>();
		TreeMap<Integer, Double> vector1 = new TreeMap<Integer, Double>(); 
		TreeMap<Integer, Double> vector2 = new TreeMap<Integer, Double>();
		ArrayList<Integer> movies = new ArrayList<Integer>();
		String xmovieGenresFile = new File("").getAbsolutePath()+"/Xmovie_genres.csv";
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ",";
		br = new BufferedReader(new FileReader(xmovieGenresFile));
		while ((line = br.readLine()) != null) {

	        // use comma as separator
		String[] USER_ID = line.split(cvsSplitBy);
		
		if(movieGenres.containsKey(USER_ID[2].replace("\"", "")))
		{
			
			movieGenres.get(USER_ID[2].replace("\"", "")).add(Integer.valueOf(USER_ID[1].replace("\"", "")));
		}
		else
		{
			System.out.println(USER_ID[2].replace("\"", ""));
			ArrayList<Integer> moviesContainingCurrentGenre = new ArrayList<Integer>();
			moviesContainingCurrentGenre.add(Integer.valueOf(USER_ID[1].replace("\"", "")));
			
			movieGenres.put(USER_ID[2].replace("\"", ""),moviesContainingCurrentGenre);
		}
		
		}
		br = new BufferedReader(new FileReader(xmovieGenresFile));
		while ((line = br.readLine()) != null) {
			String[] USER_ID = line.split(cvsSplitBy);
			if(!movies.contains(Integer.valueOf(USER_ID[1].replace("\"", ""))))
			{
				movies.add(Integer.valueOf(USER_ID[1].replace("\"", "")));
			}
		}
		br.close();
		System.out.println("To plithws twn diaforetikwn genres einai : " + movieGenres);
		System.out.println("To plithws twn diaforetikwn movies einai : " + movies);
		
		for(String key: movieGenres.keySet())
		{
			for(int i=0;i<movies.size();i++)
			{
				
				vector1.put(movies.get(i), 1.00);
				
			}
			double[] genre1Vector = new double[vector1.size()];
			
			for(int z=0;z<movies.size();z++)
			{
				if(movieGenres.get(key).contains(movies.get(z)))
				{
					vector1.put(movies.get(z), 2.00);
				}
				}
				int counter =0;
				for( int vectorKey:vector1.keySet())
				{
					
					genre1Vector[counter] = vector1.get(vectorKey);
					counter++;
				}
			for(String key2: movieGenres.keySet())
			{
				System.out.println("Sygkrinoume to "+key+" me to "+key2);
				System.out.println("me arraylist"+ movieGenres.get(key) +" kai arraylist "+movieGenres.get(key2));
				for(int i=0;i<movies.size();i++)
				{
					
					
					vector2.put(movies.get(i), 1.00);
				}
				double[] genre2Vector = new double[vector2.size()];
				for(int z=0;z<movies.size();z++)
				{
					if(movieGenres.get(key2).contains(movies.get(z)))
					{
						vector2.put(movies.get(z), 2.00);
					}
				}
					 counter =0;
					Arrays.fill(genre2Vector, 1.00);
					
					for( int vectorKey:vector2.keySet())
					{
						genre2Vector[counter] = vector2.get(vectorKey);
						counter++;
					}
				
				
				perGenreSimilarity.put(key2, calculateCosineSimilarity(genre1Vector, genre2Vector));
				
			}
		
			genreSimilarity.put(key,perGenreSimilarity);
			
		}
		PrintWriter writer = new PrintWriter("genreSimilarity.txt", "UTF-8");
		for(String key:genreSimilarity.keySet())
		{
			HashMap<String, Double>  perGenreSimilarityToWrite = genreSimilarity.get(key);
			for(String key2:perGenreSimilarityToWrite.keySet())
				writer.println(key+","+key2+","+perGenreSimilarityToWrite.get(key2));	
		}
		
		
		writer.close();
	}
	private static double calculateCosineSimilarity(double[] v1, double[] v2) {
		double dotProduct = 0.0;
		double normA = 0.0;
		double normB = 0.0;
	//	System.out.println("To v1 eiani :"+v1);
	//	System.out.println("To v2 eiani :"+v2);
		for (int i = 0; i < v1.length; i++) {
			if(v1[i]==v2[i] && v1[i] ==1.00)
			System.out.println("To v1 einai "+ v1[i]+ " enw yo v2 einai : "+v2[i]);
			dotProduct += v1[i] * v2[i];
			// System.out.println (dotProduct);
			normA += Math.pow(v1[i], 2);
			normB += Math.pow(v2[i], 2);
		}
		return dotProduct / (Math.sqrt(normA) * Math.sqrt(normB));
	}
}

